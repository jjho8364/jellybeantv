package com.keren.tissuereplytv.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.keren.tissuereplytv.R;
import com.keren.tissuereplytv.fragment.Fragment01;
import com.keren.tissuereplytv.fragment.Fragment05;
import com.keren.tissuereplytv.fragment.FragmentBayIld;
import com.keren.tissuereplytv.fragment.FragmentKorMid;
import com.keren.tissuereplytv.fragment.FragmentKorMubi;
import com.keren.tissuereplytv.fragment.FragmentLinkMid;
import com.keren.tissuereplytv.fragment.FragmentLive;
import com.keren.tissuereplytv.fragment.FragmentMaru;
import com.keren.tissuereplytv.fragment.FragmentMaruSearch;
import com.squareup.picasso.Picasso;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private final String TAG = " MainActivityTAG - ";

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FOUR = 3;
    public final static int FRAGMENT_FIVE = 4;
    public final static int FRAGMENT_TWENTYONE = 20;
    public final static int FRAGMENT_TWENTYTWO = 21;
    public final static int FRAGMENT_TWENTYTHREE = 22;
    public final static int FRAGMENT_TWENTYFOUR = 23;
    public final static int FRAGMENT_TWENTYFIVE = 24;
    public final static int FRAGMENT_MARU_DRAMA = 26;
    public final static int FRAGMENT_MARU_ENTER = 27;
    public final static int FRAGMENT_MARU_SEARCH = 28;
    public final static int FRAGMENT_BAY_MUBI = 29;
    public final static int FRAGMENT_BAY_ILD = 30;
    public final static int FRAGMENT_LINK_MID = 31;
    public final static int FRAGMENT_LINK_JOONGD = 32;
    public final static int FRAGMENT_ATKOR_MUBI = 33;
    public final static int FRAGMENT_ATKOR_MID = 34;
    public final static int FRAGMENT_KOR_MUBI_HAN = 35;
    public final static int FRAGMENT_KOR_MUBI_OVER = 36;
    public final static int FRAGMENT_KOR_MID = 37;
    public final static int FRAGMENT_KOR_CD = 38;
    public final static int FRAGMENT_KOR_JD = 39;

    private TextView tv_fragment01;
    private TextView tv_fragment02;
    private TextView tv_fragment03;
    private TextView tv_fragment04;
    private TextView tv_fragmentCableMovie;
    private TextView tv_fragmentSportsGame;
    private TextView tv_fragmentDirecrWatch;
    private TextView tv_fragmentLiveEtc;
    private TextView tv_fragmentLiveEnd;
    private TextView tv_fragmentMaruDrama;
    private TextView tv_fragmentMaruEnter;
    private TextView tv_fragmentMaruSearh;
    private TextView tv_fragmentBayMubi;
    private TextView tv_fragmentBayIld;
    private TextView tv_fragmentLinkMid;
    private TextView tv_fragmentLinkJoongd;
    private TextView tv_fragmentAtkorMubi;
    private TextView tv_fragmentAtkorMid;
    private TextView tv_fragmentKorMubiHan;
    private TextView tv_fragmentKorMubiOver;
    private TextView tv_fragmentKorMid;
    private TextView tv_fragmentKorCd;
    private TextView tv_fragmentKorJd;

    private String fragment01Url = "";
    private String fragment02Url = "";
    private String fragment03Url = "";
    private String fragment04Url = "";
    private String fragment05Url = "";
    private String fragmentCableMovieUrl;
    private String fragmentSportsGameUrl;
    private String fragmentDirecrWatchUrl;
    private String fragmentLiveEtcUrl;
    private String fragmentLiveEndUrl;
    private String fragmentMaruDrama;
    private String fragmentMaruEnter;
    private String fragmentMaruSearch;
    private String fragmentBayMubi;
    private String fragmentBayIld;
    private String fragmentLinkMid;
    private String fragmentLinkJoongd;
    private String fragmentAtkorMubi;
    private String fragmentAtkorMid;
    private String fragmentKorMubiHan;
    private String fragmentKorMubiOver;
    private String fragmentKorMid;
    private String fragmentKorCd;
    private String fragmentKorJd;

    private String nextAppUrl = "";
    private String mxPlayerUrl = "";
    private Date curDate;
    private String appStatus = "99";

    private int adsCnt = 0;
    InterstitialAd interstitialAd;
    InterstitialAd interstitialAd3;

    Date d1;
    Date d2;

    Date installedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, "ca-app-pub-9440374750128282~2072335315");

        Intent mainIntent = getIntent();
        appStatus = mainIntent.getStringExtra("appStatus");
        Log.d(TAG, "appStatus : " + appStatus);

        d1 = new Date();

        SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String first = pref.getString("first",null);
        if(first != null && !appStatus.equals("3") && !appStatus.equals("4"))  appStatus = "1";

        if(appStatus.equals("1")){
            setContentView(R.layout.activity_main);

            curDate = new Date();

            AdView adView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);

            AdRequest adRequest2 = new AdRequest.Builder().build();
            interstitialAd = new InterstitialAd(this);
            interstitialAd.setAdUnitId(getResources().getString(R.string.full_mid));
            interstitialAd.loadAd(adRequest2);

            tv_fragment01 = (TextView)findViewById(R.id.tv_fragment01);
            tv_fragment02 = (TextView)findViewById(R.id.tv_fragment02);
            tv_fragment03 = (TextView)findViewById(R.id.tv_fragment03);
            tv_fragment04 = (TextView)findViewById(R.id.tv_fragment04);
            tv_fragmentCableMovie = (TextView)findViewById(R.id.tv_fragment_cable_movie);
            tv_fragmentSportsGame = (TextView)findViewById(R.id.tv_fragment_sports_game);
            tv_fragmentDirecrWatch = (TextView)findViewById(R.id.tv_fragment_direct_watch);
            tv_fragmentLiveEtc = (TextView)findViewById(R.id.tv_fragment_live_etc);
            tv_fragmentLiveEnd = (TextView)findViewById(R.id.tv_fragment_live_end);
            tv_fragmentMaruDrama = (TextView)findViewById(R.id.tv_frag_maru_drama);
            tv_fragmentMaruEnter = (TextView)findViewById(R.id.tv_frag_maru_enter);
            tv_fragmentMaruSearh = (TextView)findViewById(R.id.tv_frag_maru_search);
            tv_fragmentBayMubi = (TextView)findViewById(R.id.tv_fragment_bay_mubi);
            tv_fragmentBayIld = (TextView)findViewById(R.id.tv_fragment_bay_ild);
            tv_fragmentLinkMid = (TextView)findViewById(R.id.tv_frag_link_mid);
            tv_fragmentLinkJoongd = (TextView)findViewById(R.id.tv_frag_link_joongd);
            tv_fragmentAtkorMubi = (TextView)findViewById(R.id.tv_frag_atkor_mubi);
            tv_fragmentAtkorMid = (TextView)findViewById(R.id.tv_frag_atkor_mid);
            tv_fragmentKorMubiHan = (TextView)findViewById(R.id.tv_frag_kor_mubi_han);
            tv_fragmentKorMubiOver = (TextView)findViewById(R.id.tv_frag_kor_mubi_over);
            tv_fragmentKorMid = (TextView)findViewById(R.id.tv_frag_kor_mid);
            tv_fragmentKorCd = (TextView)findViewById(R.id.tv_frag_kor_cd);
            tv_fragmentKorJd = (TextView)findViewById(R.id.tv_frag_kor_jd);

            tv_fragment01.setOnClickListener(this);
            tv_fragment02.setOnClickListener(this);
            tv_fragment03.setOnClickListener(this);
            tv_fragment04.setOnClickListener(this);
            tv_fragmentCableMovie.setOnClickListener(this);
            tv_fragmentSportsGame.setOnClickListener(this);
            tv_fragmentDirecrWatch.setOnClickListener(this);
            tv_fragmentLiveEtc.setOnClickListener(this);
            tv_fragmentLiveEnd.setOnClickListener(this);
            tv_fragmentMaruDrama.setOnClickListener(this);
            tv_fragmentMaruEnter.setOnClickListener(this);
            tv_fragmentMaruSearh.setOnClickListener(this);
            tv_fragmentBayMubi.setOnClickListener(this);
            tv_fragmentBayIld.setOnClickListener(this);
            tv_fragmentLinkMid.setOnClickListener(this);
            tv_fragmentLinkJoongd.setOnClickListener(this);
            tv_fragmentAtkorMubi.setOnClickListener(this);
            tv_fragmentAtkorMid.setOnClickListener(this);
            tv_fragmentKorMubiHan.setOnClickListener(this);
            tv_fragmentKorMubiOver.setOnClickListener(this);
            tv_fragmentKorMid.setOnClickListener(this);
            tv_fragmentKorCd.setOnClickListener(this);
            tv_fragmentKorJd.setOnClickListener(this);

            fragment01Url = mainIntent.getStringExtra("fragment01Url");
            fragment02Url = mainIntent.getStringExtra("fragment02Url");
            fragment03Url = mainIntent.getStringExtra("fragment03Url");
            fragment04Url = mainIntent.getStringExtra("fragment04Url");
            fragment05Url = mainIntent.getStringExtra("fragment05Url");
            fragmentCableMovieUrl = mainIntent.getStringExtra("fragmentCableMovieUrl");
            fragmentSportsGameUrl = mainIntent.getStringExtra("fragmentSportsGameUrl");
            fragmentDirecrWatchUrl = mainIntent.getStringExtra("fragmentDirectWatchUrl");
            fragmentLiveEtcUrl = mainIntent.getStringExtra("fragmentLiveEtcUrl");
            fragmentLiveEndUrl = mainIntent.getStringExtra("fragmentLiveEndUrl");
            fragmentMaruDrama = mainIntent.getStringExtra("fragMaruDrama2");
            fragmentMaruEnter = mainIntent.getStringExtra("fragMaruEnter2");
            fragmentMaruSearch = mainIntent.getStringExtra("fragMaruSearch");
            fragmentBayMubi = mainIntent.getStringExtra("fragBayMubi");
            fragmentBayIld = mainIntent.getStringExtra("fragBayIld");
            fragmentLinkMid = mainIntent.getStringExtra("fragLinkMid");
            fragmentLinkJoongd = mainIntent.getStringExtra("fragLinkJoongd");
            fragmentAtkorMubi = mainIntent.getStringExtra("fragAtkorMubi");
            fragmentAtkorMid = mainIntent.getStringExtra("fragAtkorMid");
            fragmentKorMubiHan = mainIntent.getStringExtra("fragKorMubiHan");
            fragmentKorMubiOver = mainIntent.getStringExtra("fragKorMubiOver");
            fragmentKorMid = mainIntent.getStringExtra("fragKorMid");
            fragmentKorCd = mainIntent.getStringExtra("fragKorCd");
            fragmentKorJd = mainIntent.getStringExtra("fragKorJd");

            mCurrentFragmentIndex = FRAGMENT_MARU_SEARCH;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.

            editor.commit(); //완료한다.

        } else if(appStatus.equals("2")){
            setContentView(R.layout.maintenance);
            String maintenanceImgUrl = mainIntent.getStringExtra("maintenance");
            ImageView imgView = (ImageView)findViewById(R.id.img_maintenance);
            if(maintenanceImgUrl == null || maintenanceImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(maintenanceImgUrl).into(imgView);
            }
        } else if(appStatus.equals("3")){
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closed");
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            if(closedImgUrl == null || closedImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(closedImgUrl).into(imgView);
            }
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(this);
        } else if(appStatus.equals("4")) {   // mx player
            setContentView(R.layout.mxplayer);
            mxPlayerUrl = mainIntent.getStringExtra("mxPlayerUrl");
            Button btnMxPlayer = (Button) findViewById(R.id.btn_mxplayer);
            btnMxPlayer.setOnClickListener(this);
        } else if(appStatus.equals("9")) {   // mx player
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closedImgUrl");
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            Log.d(TAG, "  " + nextAppUrl);
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            Picasso.with(this).load(closedImgUrl).into(imgView);
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                    marketLaunch1.setData(Uri.parse(nextAppUrl));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(nextAppUrl));
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tv_fragment01:
                offColorTv();
                tv_fragment01.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment02:
                offColorTv();
                tv_fragment02.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment03:
                offColorTv();
                tv_fragment03.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment04:
                offColorTv();
                tv_fragment04.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_cable_movie:
                offColorTv();
                tv_fragmentCableMovie.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_sports_game:
                offColorTv();
                tv_fragmentSportsGame.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYTWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_direct_watch:
                offColorTv();
                tv_fragmentDirecrWatch.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYTHREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_live_etc:
                offColorTv();
                tv_fragmentLiveEtc.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYFOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_live_end:
                offColorTv();
                tv_fragmentLiveEnd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWENTYFIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_drama:
                offColorTv();
                tv_fragmentMaruDrama.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_MARU_DRAMA;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_enter:
                offColorTv();
                tv_fragmentMaruEnter.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_MARU_ENTER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_search:
                offColorTv();
                tv_fragmentMaruSearh.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_MARU_SEARCH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_bay_mubi:
                offColorTv();
                tv_fragmentBayMubi.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_MUBI;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_bay_ild:
                offColorTv();
                tv_fragmentBayIld.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_ILD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_link_mid:
                offColorTv();
                tv_fragmentLinkMid.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LINK_MID;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_link_joongd:
                offColorTv();
                tv_fragmentLinkJoongd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LINK_JOONGD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_mubi:
                offColorTv();
                tv_fragmentAtkorMubi.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_MUBI;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_mid:
                offColorTv();
                tv_fragmentAtkorMid.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_MID;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mubi_han:
                offColorTv();
                tv_fragmentKorMubiHan.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MUBI_HAN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mubi_over:
                offColorTv();
                tv_fragmentKorMubiOver.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MUBI_OVER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mid:
                offColorTv();
                tv_fragmentKorMid.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MID;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_cd:
                offColorTv();
                tv_fragmentKorCd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_CD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_jd:
                offColorTv();
                tv_fragmentKorJd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_JD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.btn_closed:
                Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                Log.d(TAG, "  " + nextAppUrl);
                marketLaunch1.setData(Uri.parse(nextAppUrl));
                startActivity(marketLaunch1);
                break;
            case R.id.btn_mxplayer:
                Intent marketLaunch2 = new Intent(Intent.ACTION_VIEW);
                marketLaunch2.setData(Uri.parse(mxPlayerUrl));
                startActivity(marketLaunch2);
                break;
        }
    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;
        Bundle args = new Bundle();
        args.putInt("adsCnt", adsCnt);

        switch (idx) {
            case FRAGMENT_ONE:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWO:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment02Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THREE:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment03Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOUR:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment04Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FIVE:
                newFragment = new Fragment05();
                args.putString("baseUrl", fragment05Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYONE:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentCableMovieUrl);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYTWO:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentSportsGameUrl);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYTHREE:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentDirecrWatchUrl);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYFOUR:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLiveEtcUrl);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWENTYFIVE:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLiveEndUrl);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_MARU_DRAMA:
                newFragment = new FragmentMaru();
                args.putString("baseUrl", fragmentMaruDrama);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_MARU_ENTER:
                newFragment = new FragmentMaru();
                args.putString("baseUrl", fragmentMaruEnter);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_MARU_SEARCH:
                newFragment = new FragmentMaruSearch();
                args.putString("baseUrl", fragmentMaruSearch);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_BAY_MUBI:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentBayMubi);
                args.putString("type", "%7C4/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_BAY_ILD:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentBayIld);
                args.putString("type", "%7C7/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LINK_MID:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkMid);
                args.putString("type", "%7C6/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LINK_JOONGD:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkJoongd);
                args.putString("type", "%7C8/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_MUBI:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkJoongd);
                args.putString("type", "%7C8/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_MID:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkJoongd);
                args.putString("type", "%7C8/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MUBI_HAN:
                newFragment = new FragmentKorMubi();
                args.putString("baseUrl", fragmentKorMubiHan);
                args.putString("searchUrl", "&bo_table=kmovie&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MUBI_OVER:
                newFragment = new FragmentKorMubi();
                args.putString("baseUrl", fragmentKorMubiOver);
                args.putString("searchUrl", "&bo_table=engmovie&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MID:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorMid);
                args.putString("searchUrl", "&bo_table=mid&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_CD:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorCd);
                args.putString("searchUrl", "&bo_table=cd&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_JD:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorJd);
                args.putString("searchUrl", "&bo_table=jd&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            default:
                Log.d(TAG, "Unhandle case");
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
        }

        return newFragment;
    }

    public void fragmentReplace(int reqNewFragmentIndex) {
        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // replace fragment
        transaction.replace(R.id.ll_fragment, newFragment);
        transaction.commit();   // Commit the transaction
    }

    public void offColorTv(){
        tv_fragment01.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment02.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment03.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment04.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentCableMovie.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentSportsGame.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentDirecrWatch.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLiveEtc.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLiveEnd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentMaruDrama.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentMaruEnter.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentMaruSearh.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentBayMubi.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentBayIld.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLinkMid.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLinkJoongd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorMubi.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorMid.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMubiHan.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMubiOver.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMid.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorCd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorJd.setBackgroundResource(R.drawable.fragment_borther);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "main activity onResume");

        if(appStatus.equals("1") && d1 != null){
            d2 = new Date();
            long diff = d2.getTime() - d1.getTime();
            long min = diff / 1000 / 60 ;
            Log.d(TAG, "min : " + min);
            if(min >= 30){
                d1 = new Date();
                AdRequest adRequest3 = new AdRequest.Builder().build();
                interstitialAd3 = new InterstitialAd(this);
                interstitialAd3.setAdUnitId(getResources().getString(R.string.full_end));
                interstitialAd3.loadAd(adRequest3);
                interstitialAd3.setAdListener(new AdListener(){
                    @Override public void onAdLoaded() {
                        if (interstitialAd3.isLoaded()) {
                            interstitialAd3.show();
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setTitle("안녕히 가세요.")
                .setMessage("종료 하시겠습니까?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // 확인시 처리 로직
                        if(appStatus.equals("1")){
                            interstitialAd.show();
                        }
                        finish();
                    }})
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // 취소시 처리 로직
                        return;
                    }})
                .show();
    }
}
