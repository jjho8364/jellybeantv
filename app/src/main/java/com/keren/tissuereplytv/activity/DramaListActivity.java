package com.keren.tissuereplytv.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.keren.tissuereplytv.R;
import com.keren.tissuereplytv.adapter.DramaBtnAdapter;
import com.keren.tissuereplytv.adapter.GridDramaAdapter;
import com.keren.tissuereplytv.item.GridDramaItem;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class DramaListActivity extends Activity {
    private String TAG = " DramaListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    //private ImageView img_poster;

    private ArrayList<GridDramaItem> listViewItemArr;
    private GridView gridView;
    private ListView btnListView;

    private String nextUrl = "";
    private GetPlayer getPlayer;

    private int adsCnt = 0;
    private String listUrl = "";

    // private adveirtise
    private ImageView imgPrAds;
    String adsLink = "";
    String adsImgUrl = "";

    // admax
    String runUrl = "";

    public ImageView ly_image;
    public ImageView iv_left_image;
    public TextView memo_right1;
    public TextView memo_right2;
    public TextView memo_brand;
    public TextView memo_under;

    public TextView btn01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_drama_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        /*SharedPreferences adsPref= getSharedPreferences("adsPref", MODE_PRIVATE); // 선언
        String ads1 = adsPref.getString("ads1",null);
        Log.d(TAG, "ads1 : " + ads1);
        if(ads1 != null && !ads1.equals("")){
            String[] arrAds1 = ads1.split(",");
            adsImgUrl = arrAds1[0];
            adsLink = arrAds1[1];
            title = arrAds1[2];

            if (adsImgUrl != null && !adsImgUrl.equals("")){
                Picasso.with(this).load(adsImgUrl).into(imgPrAds);
            }

            imgPrAds.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(adsLink));
                    startActivity(intent);
                }
            });
        }*/

        ly_image = (ImageView)findViewById(R.id.ly_image);
        iv_left_image = (ImageView)findViewById(R.id.iv_left_image);
        memo_right1 = (TextView) findViewById(R.id.memo_right1);
        memo_right2 = (TextView)findViewById(R.id.memo_right2);
        memo_brand = (TextView)findViewById(R.id.memo_brand);
        memo_under = (TextView)findViewById(R.id.memo_under);
        btn01 = (TextView)findViewById(R.id.tv_btn);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        //title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));


        //tv_title = (TextView)findViewById(R.id.tv_title);
        //img_poster  = (ImageView)findViewById(R.id.img_poster);

        //tv_title.setText(title);



        /*if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }*/

        gridView = (GridView)findViewById(R.id.gridview);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getGridView = new GetGridView();
        getGridView.execute();

    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        ArrayList<String> btnTextArr = new ArrayList<String>();
        ArrayList<String> btnVideoUrlArr = new ArrayList<String>();

        String str_ly_image = "";
        String str_iv_left_image = "";
        String str_memo_right1 = "";
        String str_memo_right2 = "";
        String str_memo_brand = "";
        String str_memo_under = "";
        String str_link = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridDramaItem>();

            mProgressDialog = new ProgressDialog(DramaListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                Elements lists = doc.select(".carousel-inner .item.active .item");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).select("h3 a").attr("title");
                    String update = lists.get(i).select(".date").text();
                    String imgUrl = lists.get(i).select(".item-img a img").attr("src");
                    String listUrl = lists.get(i).select(".item-img a").attr("href");

                    GridDramaItem gridDramaItem = new GridDramaItem(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridDramaItem);
                }

                /////////////// video button //////////////
                Elements btns = doc.select(".pagination.post-tape a");

                for(int i=0 ; i<btns.size() ; i++){
                    String text = btns.get(i).text();
                    String videoUrl = btns.get(i).attr("href");

                    if(videoUrl.contains(".daum.net/")) continue;
                    if(videoUrl.contains("hqvid.net")) continue;
                    if(videoUrl.contains("goovid.net")) continue;
                    //if(videoUrl.contains("openload")) continue;
                    btnTextArr.add(text);
                    btnVideoUrlArr.add(videoUrl);
                }

                if(btnTextArr.size() == 0){
                    String btnVideoUrl = doc.select(".player iframe").attr("src");
                    Log.d(TAG, "btnVideoUrl : " + btnVideoUrl);

                    if(btnVideoUrl != null && !btnVideoUrl.equals("")) {
                        btnTextArr.add("링크 1");
                        btnVideoUrlArr.add(baseUrl);
                    }
                }

                if(btnTextArr.size() == 0){
                    Elements btnLists = doc.select(".div-container .div-center a");

                    for(int i=0 ; i<btnLists.size() ; i++) {
                        String text = btnLists.get(i).text();
                        String videoUrl = btnLists.get(i).attr("href");

                        btnTextArr.add(text);
                        btnVideoUrlArr.add(videoUrl);
                    }
                }

                ///////////// adverties ////////////////
                doc = Jsoup.connect("https://freekinglivetv.wordpress.com/kdrama2/").timeout(10000).get();

                Elements adsLists = doc.select(".beauty");
                int itemCnt = adsLists.size();

                int rndNum = (int)(Math.random() * itemCnt);

                str_ly_image = adsLists.get(rndNum).select(".ly_image").text()+"";
                str_iv_left_image = adsLists.get(rndNum).select(".iv_left_image").text()+"";
                str_memo_right1 = adsLists.get(rndNum).select(".memo_right1").text()+"";
                str_memo_right2 = adsLists.get(rndNum).select(".memo_right2").text()+"";
                str_memo_brand = adsLists.get(rndNum).select(".memo_brand").text()+"";
                str_memo_under = adsLists.get(rndNum).select(".memo_under").text()+"";
                str_link = adsLists.get(rndNum).select(".link").text()+"";

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(DramaListActivity.this != null && listViewItemArr.size() != 0){
                gridView.setAdapter(new GridDramaAdapter(DramaListActivity.this, listViewItemArr, R.layout.item_grid_drama));

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        adsCnt++;
                        Log.d(TAG, "adsCnt : " + adsCnt);
                        listUrl = listViewItemArr.get(position).getListUrl();
                        title = listViewItemArr.get(position).getTitle();
                        imgUrl = listViewItemArr.get(position).getImgUrl();
                        refresh(listUrl);
                    }
                });
            }

            ///////// set button list ////////////
            if(btnTextArr.size() == 0){
                Toast.makeText(DramaListActivity.this, "방송 준비 중입니다. 1시간 내에 완료됩니다.", Toast.LENGTH_SHORT).show();
            }
            btnListView.setAdapter(new DramaBtnAdapter(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);
                    getPlayer = new GetPlayer();
                    getPlayer.execute();
                }
            });

            Log.d(TAG,"str_link : " + str_link);
            if(str_link != null && !str_link.equals("")){
                if(str_link.split(",")[1].equals("2")){
                    memo_right1.setTextColor(Color.WHITE);
                    memo_right2.setTextColor(Color.WHITE);
                    memo_brand.setTextColor(Color.WHITE);
                    memo_under.setTextColor(Color.WHITE);
                }
                if(!str_ly_image.equals("")) Picasso.with(getApplicationContext()).load(str_ly_image).into(ly_image);
                if(!str_iv_left_image.equals("")) Picasso.with(getApplicationContext()).load(str_iv_left_image).into(iv_left_image);
                if(!str_memo_right1.equals("")) memo_right1.setText(str_memo_right1);
                if(!str_memo_right2.equals("")) memo_right2.setText(str_memo_right2);
                if(!str_memo_brand.equals("")) memo_brand.setText(str_memo_brand);
                if(!str_memo_under.equals("")) memo_under.setText(str_memo_under);
                btn01.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(str_link.split(",")[0]));
                        startActivity(intent);
                    }
                });
            } else {

            }

            mProgressDialog.dismiss();
        }
    }

    public class GetPlayer extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(DramaListActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();
                playerUrl = doc.select(".player iframe").attr("src");

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".div-container .div-center a").attr("href");
                }

                if(playerUrl.contains("media.videomovil")){
                    doc = Jsoup.connect(playerUrl).timeout(15000).get();
                    playerUrl = doc.select("iframe").first().attr("src");
                }

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".player a").attr("href");
                }

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = nextUrl;
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Intent intent = new Intent(DramaListActivity.this, WebviewPlayerActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);

            mProgressDialog.dismiss();
        }
    }

    public void refresh(String listUrl){
        Intent intent = new Intent(DramaListActivity.this, DramaListActivity.class);
        intent.putExtra("listUrl", listUrl);
        intent.putExtra("title", title);
        intent.putExtra("imgUrl", imgUrl);
        intent.putExtra("adsCnt", ""+adsCnt);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getGridView != null){
            getGridView.cancel(true);
        }
        if(getPlayer != null){
            getPlayer.cancel(true);
        }
    }

}
