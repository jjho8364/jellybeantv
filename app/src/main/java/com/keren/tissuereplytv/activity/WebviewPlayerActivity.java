package com.keren.tissuereplytv.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.keren.tissuereplytv.R;
import com.keren.tissuereplytv.adapter.GridDramaAdapter;
import com.keren.tissuereplytv.fragment.Fragment01;
import com.keren.tissuereplytv.item.GridDramaItem;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;

public class WebviewPlayerActivity extends Activity {
    private String TAG = "WebviewPlayerActivity - ";
    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;
    private String baseUrl = "";
    private String javascriptMenu = "";
    private String javascriptId = "";
    private boolean autoCek = true;
    private boolean palygoogleFlg = true;
    private boolean srchStart = true;
    private boolean canPlayFlg = true;
    private boolean webViewFlg = true;

    private ArrayList<String> keyArr;
    private JSONObject jsonObj;
    private String javascript = "";
    private String fileType = ".m3u8";
    private String dataType = "";
    private String[] arrFileType;
    private String[] arrWebViewList;
    private String[] arrStopPlay;
    private String[] arrNeedMx;
    private String decodeMode = "";
    GetJson getJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_webview_player);

        baseUrl = getIntent().getStringExtra("baseUrl");

        //ImageView loadingImg = (ImageView)findViewById(R.id.gif_dog);
        //GlideDrawableImageViewTarget gifImage = new GlideDrawableImageViewTarget(loadingImg);
        //Glide.with(this).load(R.drawable.sleepy_dog).into(gifImage);

        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);

        //webView = (WebView) findViewById(R.id.webView);
        webView = new WebView(getApplicationContext());
        customViewContainer.addView(webView);

        mWebViewClient = new myWebViewClient();
        //webView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true); // prevent popup
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAppCacheMaxSize( 10 * 1024 * 1024 );
        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath() );
        webView.getSettings().setAllowFileAccess( true );
        webView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );
        //webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setBackgroundColor(0x00000000);
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");

        //String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
        //webView.getSettings().setUserAgentString(newUA);

        webView
                .getSettings().setMediaPlaybackRequiresUserGesture(false);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

                if(autoCek){
                    Log.d(TAG, "all url : " + url);
                }

                if(url.contains(".gif") || url.contains(".png") || url.contains(".jpg") || url.contains("ads") || url.contains(".php")){
                    return null;
                }


                if(autoCek){
                    for(int i=0 ; i<arrFileType.length ; i++){
                        if(url.contains(arrFileType[i])){
                            Log.d(TAG, "contain fileType : " + arrFileType[i]);
                            startMxPlayer(url, dataType, decodeMode);
                        }
                    }
                }
                /*if(autoCek && url.contains(fileType)) {
                    Log.d(TAG, "contain fileType : " + fileType);
                    startMxPlayer(url, dataType);
                }*/

                return null;
            }
            /////////////////////////////////////////////////////////////////////////////////////
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click..." + url);
                /*if(url.contains("gdriveplayer.co") || url.contains("flashvid.net") ){
                    Log.i(TAG, "alert : executed shouldOverrideUrlLoading : " + url);
                    webView.stopLoading();
                    webView.loadUrl(url);
                }*/
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                //super.onPageFinished(webView, url);

                if(srchStart){
                    srchStart = false;  // block popup
                    Log.d(TAG, "alert : executed onPageFinished : " + url);
                    Log.d(TAG, "typing javascript : " + javascript);
                    autoCek = true;
                    webView.loadUrl("javascript:" + javascript);

                    /*Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl("javascript:window.Android.getHtml(document.getElementsByTagName('html')[0].innerHTML);");
                        }
                    }, 1000);*/
                }
                //webView.loadUrl("javascript:window.Android.getHtml(document.getElementsByTagName('html')[0].innerHTML);");
            }
        });

        Log.d(TAG, "baseUrl2 : " + baseUrl);
        baseUrl = addHttps(baseUrl);

        Intent intent = new Intent(WebviewPlayerActivity.this, VideoPlayerActivity.class);
        intent.putExtra("baseUrl", baseUrl);
        startActivity(intent);

        //getJson = new GetJson();
        //getJson.execute();


    }

    public class GetJson extends AsyncTask<Void, Void, Void> {

        //String[] arrTempFileType;
        String videoJsUrl = "";
        String jwPlayerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            keyArr = new ArrayList<String>();

        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            Document kVidDoc = null;
            Document setDoc = null;

            try {

                // mubi inner iframe
                doc = Jsoup.connect(baseUrl).timeout(20000).get();
                String innerIframe = doc.select("iframe").attr("src");
                if(innerIframe != null && !innerIframe.equals("")){
                    baseUrl = innerIframe;
                }

                setDoc = Jsoup.connect("https://freekinglivetv.wordpress.com/kdrama2/").timeout(20000).get();
                //setDoc = Jsoup.connect("https://freekinglivetv.wordpress.com/drama3/").timeout(20000).get();
                String strJson = setDoc.select(".json_player").text().replace("`", "\"");
                jsonObj = new JSONObject(strJson);
                Iterator<String> strKey = jsonObj.keys();
                while(strKey.hasNext()){
                    keyArr.add(strKey.next().toString());
                }

                arrFileType = setDoc.select(".fileType").text().split(",");
                videoJsUrl = setDoc.select("#my-video source").attr("src");
                jwPlayerUrl = setDoc.select("video").attr("src");
                arrWebViewList = setDoc.select(".webView").text().split(",");
                arrStopPlay = setDoc.select(".stopPlay").text().split(",");
                arrNeedMx = setDoc.select(".needMx").text().split(",");

            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                for(int i=0 ; i<keyArr.size() ; i++){
                    if(baseUrl.contains(keyArr.get(i))){
                        String[] tempSplit = jsonObj.get(keyArr.get(i)).toString().split(",");
                        String scriptType = tempSplit[0];
                        //if()

                        javascript = scriptType.equals("1") ? "$('" + tempSplit[1] + "').click()" : tempSplit[1];
                        //fileType = tempSplit[2];
                        dataType = tempSplit[2];
                        decodeMode = tempSplit[3];
                        break;
                    }
                }

                if(dataType.equals("")){
                    Log.d(TAG, "alert : no have dataType..");
                    javascript = "$('div').click()";
                    //fileType = ".mp4";
                    dataType = "html";
                    arrFileType = new String[]{".mp4","googlevideo.com",".m3u8","googleusercontent.com"};
                    decodeMode = "2";
                    arrWebViewList = new String[]{"azvideo"};
                    arrStopPlay = new String[]{"supervid"};
                    arrNeedMx = new String[]{"flashvid.net","gdriveplayer"};

                    Log.d(TAG, "arrFileType : " + arrFileType[1]);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            Log.d(TAG, "get javascript : " + javascript);
            //Log.d(TAG, "get fileType : " + fileType);
            Log.d(TAG, "get dataType : " + dataType);
            Log.d(TAG, "get decodeMode : " + decodeMode);
            Log.d(TAG, "arrFileType : " + arrFileType[0]);
            Log.d(TAG, "arrStopPlay : " + arrStopPlay[0]);
            Log.d(TAG, "arrNeedMx : " + arrNeedMx[1]);

            // start webview
            for(int i=0 ; i<arrNeedMx.length ; i++){
                Log.d(TAG, "arrNeedMx[" + i + "] : " + arrNeedMx[i]);
                if(baseUrl.contains(arrNeedMx[i])) webViewFlg = false;
            }

            // block link
            for(int i=0 ; i<arrStopPlay.length ; i++){
                if(baseUrl.contains(arrStopPlay[i])){
                    canPlayFlg = false;
                    Toast.makeText(WebviewPlayerActivity.this, "이 링크는 사용할 수 없습니다.", Toast.LENGTH_LONG).show();
                }
            }

            if(dataType.equals("html")){
                Toast.makeText(WebviewPlayerActivity.this, "이 링크는 광고를 제거 할 수 없습니다.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(baseUrl));
                startActivity(intent);
            } else if(webViewFlg && canPlayFlg){
                Toast.makeText(WebviewPlayerActivity.this, "전체화면으로 감상 하세요. 작은 팝업광고는 닫아 주세요.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(WebviewPlayerActivity.this, VideoPlayerActivity.class);
                intent.putExtra("baseUrl", baseUrl);
                startActivity(intent);
            } else {
                Log.d(TAG, "executed baseUrl : " + baseUrl);
                if(baseUrl.contains("gdriveplayer") || baseUrl.contains("jetload")){
                    Toast.makeText(WebviewPlayerActivity.this, "플레이 버튼을 눌러주세요.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(WebviewPlayerActivity.this, "자동 실행 되오니 클릭하지 마세요.", Toast.LENGTH_LONG).show();
                }

                if(canPlayFlg) webView.loadUrl(baseUrl);   // finish prepare

            }
        }
    }

    public class MyJavascriptInterface {
        @JavascriptInterface
        public void getHtml(String html) { //위 자바스크립트가 호출되면 여기로 html이 반환됨
            //Log.d(TAG, "html : " + html);
            Document doc = null;

            try {
                doc = Jsoup.parse(html);

                String gdriveUrl = doc.select("video").attr("src");
                startMxPlayer(gdriveUrl, dataType, decodeMode);
                // 광고 없는 JWVIDEO
                //String videoUrl = doc.select("video").attr("src");
                //Log.d(TAG, "videoUrl : " + videoUrl);

                /*
                Elements lists = doc.select("video");
                for (int i=0 ; i<lists.size() ; i++){
                    Log.d(TAG, "supervid : " + lists.get(i).html());
                }*/


                //MyRunnable obj = new MyRunnable(videoUrl);
                //handler.post(obj);

            } catch(Exception e){
                e.printStackTrace();
            }


        }
    }

    public class MyRunnable implements Runnable {
        private String videoUrl;
        public MyRunnable(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        @Override
        public void run() {
            if(WebviewPlayerActivity.this != null){
                //webView.loadDataWithBaseURL(null, imgHtml, "text/html", "UTF-8", null);
                //stopOnfinish = false;
                //hideWebview.setVisibility(webView.GONE);
                startMxPlayer(videoUrl, dataType, decodeMode);

            }
        }
    }



    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        Log.d(TAG, "onPause...");
        if (webView != null){
            try {
                Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null) .invoke(webView, (Object[]) null);
            } catch(ClassNotFoundException cnfe) {
                cnfe.printStackTrace();
            } catch(NoSuchMethodException nsme) {
                nsme.printStackTrace();
            } catch(InvocationTargetException ite) {
                ite.printStackTrace();
            } catch (IllegalAccessException iae) {
                iae.printStackTrace();
            }
        }

        /*if(webView != null){
            destroyWebView();
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
        Log.d(TAG, "onResume...");
        if(webView != null) webView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
        Log.d(TAG, "onStop...");
        if (inCustomView()) {
            hideCustomView();
        }
        /*if(webView != null){
            destroyWebView();
        }*/
        if (webView != null){
            try {
                Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null) .invoke(webView, (Object[]) null);
            } catch(ClassNotFoundException cnfe) {
                cnfe.printStackTrace();
            } catch(NoSuchMethodException nsme) {
                nsme.printStackTrace();
            } catch(InvocationTargetException ite) {
                ite.printStackTrace();
            } catch (IllegalAccessException iae) {
                iae.printStackTrace();
            }
        }
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && webView != null && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.cancel();
            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            result.cancel();
            return true;
        }

        @Override
        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
            result.cancel();
            return true;
        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            String msg = consoleMessage.message()+"";
            return super.onConsoleMessage(consoleMessage);
        }

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(WebviewPlayerActivity.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //return super.shouldOverrideUrlLoading(view, url);    //To change body of overridden methods use File | Settings | File Templates.
            return true;
        }
    }

    public void startMxPlayer(String videoUrl, String dataType, String decodeMode){
        //Toast.makeText(LiveWebview.this, "2초간 종료 하지마세요.", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "startMxPlayer : " + videoUrl);
        autoCek = false;
        String packageName = "";

        Intent startLink1 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.ad");
        Intent startLink2 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.pro");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri videoUri = Uri.parse(videoUrl);
        intent.setDataAndType(videoUri, dataType);
        intent.putExtra("decode_mode", (byte) Integer.parseInt(decodeMode));
        intent.putExtra("video_zoom", 0);
        intent.putExtra("fast_mode", true);
        if (startLink2 != null) {
            packageName = "com.mxtech.videoplayer.pro";
        } else {
            packageName = "com.mxtech.videoplayer.ad";
        }
        intent.setPackage(packageName);
        startActivity(intent);
        finish();
    }


    public void destroyWebView() {
        webView.clearHistory();
        webView.clearCache(true);
        webView.loadUrl("about:blank");
        webView.onPause();
        webView.removeAllViews();
        webView.destroyDrawingCache();
        customViewContainer.removeAllViews();
        webView.destroy();
        webView = null;
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            //webView.getSettings().setJavaScriptEnabled(false);
        }
    };

    public String addHttps(String baseUrl){
        if(baseUrl.contains("https")){
            return baseUrl;
        } else if(baseUrl.contains("http")) {
            return baseUrl;
        } else {
            return "https:" + baseUrl;
        }
    };


}
