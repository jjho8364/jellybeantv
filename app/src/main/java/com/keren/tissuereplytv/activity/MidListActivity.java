package com.keren.tissuereplytv.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.keren.tissuereplytv.R;
import com.keren.tissuereplytv.adapter.MidBtnAdapter;
import com.keren.tissuereplytv.item.BeautyItem;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class MidListActivity extends Activity {
    private String TAG = " PlayListMidActivity - ";
    private ProgressDialog mProgressDialog;
    //private ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";

    private ImageView posterView;
    private TextView titleView;
    private TextView storyView;

    private ListView btnListView;

    private int adsCnt = 0;

    private String listUrl = "";
    private String nextUrl = "";
    private GetPlayerUrl getPlayerUrl;

    // private adveirtise
    private ImageView imgPrAds;
    private TextView tvPrAds;
    String adsLink = "";
    String adsImgUrl = "";
    String adsTitle = "";

    String runUrl = "";

    BeautyItem beautyItem;
    public ImageView ly_image;
    public ImageView iv_left_image;
    public TextView memo_right1;
    public TextView memo_right2;
    public TextView memo_brand;
    public TextView memo_under;
    public TextView btn01;

    public ImageView ly_image2;
    public ImageView iv_left_image2;
    public TextView memo_right12;
    public TextView memo_right22;
    public TextView memo_brand2;
    public TextView memo_under2;
    public TextView btn02;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mid_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        posterView = (ImageView)findViewById(R.id.iv_list_eps);
        titleView = (TextView)findViewById(R.id.tv_list_eps);
        storyView = (TextView)findViewById(R.id.tv_list_story);

        //listView = (ListView)findViewById(R.id.list_listview);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getListView = new GetListView();
        getListView.execute();
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String imgUrl = "";
        String title = "";
        String story = "";

        ArrayList<String> btnTextArr = new ArrayList<String>();
        List<String> btnVideoUrlArr = new ArrayList<String>();

        List<String> listTitleArr = new ArrayList<String>();
        List<String> listPageUrlArr = new ArrayList<String>();

        String str_ly_image = "";
        String str_iv_left_image = "";
        String str_memo_right1 = "";
        String str_memo_right2 = "";
        String str_memo_brand = "";
        String str_memo_under = "";
        String str_link = "";

        String str_ly_image2 = "";
        String str_iv_left_image2 = "";
        String str_memo_right12 = "";
        String str_memo_right22 = "";
        String str_memo_brand2 = "";
        String str_memo_under2 = "";
        String str_link2 = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(MidListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(30000).get();

                ////////////////// episode list //////////////
                Elements lists = doc.select(".list-subject");

                for(int i=0 ; i<lists.size() ; i++){
                    String listTitle = lists.get(i).select("a").text();
                    String listVideoUrl = lists.get(i).select("a").attr("href");
                    listTitleArr.add(listTitle);
                    listPageUrlArr.add(listVideoUrl);
                }
                //////////////// episode contents ////////////
                imgUrl = doc.select(".thumbnail img").attr("abs:src");
                title = doc.select(".thumbnail .caption h2").text();
                story = doc.select(".thumbnail p").text();

                /////////////// video button //////////////
                Elements btns = doc.select(".row .col-md-8 center a");

                for(int i=0 ; i<btns.size()-1 ; i++){
                    String btnText = btns.get(i).text();
                    String btnVideoUrl = btns.get(i).attr("href");

                    btnTextArr.add(btnText);
                    btnVideoUrlArr.add(btnVideoUrl);

                }


            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(!(imgUrl == null || imgUrl.equals(""))){
                Picasso.with(MidListActivity.this).load(imgUrl).into(posterView);
            }
            titleView.setText(title);
            storyView.setText(story);

            ///////// set button list ////////////
            btnListView.setAdapter(new MidBtnAdapter(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);
                    getPlayerUrl = new GetPlayerUrl();
                    getPlayerUrl.execute();
                }
            });

            mProgressDialog.dismiss();
        }
    }

    public class GetPlayerUrl extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(MidListActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();
                playerUrl = doc.select("iframe").attr("src");
                Log.d(TAG, "playerUrl : " + playerUrl);

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent intent;
            intent = new Intent(MidListActivity.this, WebviewPlayerActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);
            mProgressDialog.dismiss();
        }
    }

    public void refresh(String listUrl){
        Intent intent = new Intent(MidListActivity.this, MidListActivity.class);
        intent.putExtra("listUrl",  listUrl);
        intent.putExtra("adsCnt",  adsCnt+"");
        finish();
        startActivity(intent);
    }

    public void destroyAsync(){
        if(getListView != null){
            getListView.cancel(true);
        }
        if(getPlayerUrl != null){
            getPlayerUrl.cancel(true);
        }
    }
}

