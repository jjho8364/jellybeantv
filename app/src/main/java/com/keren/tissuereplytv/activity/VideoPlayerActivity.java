package com.keren.tissuereplytv.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.keren.tissuereplytv.R;

public class VideoPlayerActivity extends Activity {
    private String TAG = "VideoPlayerActivity - ";
    private WebView webView;
    private String baseUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        setContentView(R.layout.activity_video_player);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        baseUrl = getIntent().getStringExtra("baseUrl");

        webView = (WebView)findViewById(R.id.webView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true); // prevent popup
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAppCacheMaxSize( 10 * 1024 * 1024 );
        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath() );
        webView.getSettings().setAllowFileAccess( true );
        webView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setBackgroundColor(0x00000000);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        //String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
        //webView.getSettings().setUserAgentString(newUA);

        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

                return null;
            }
            /////////////////////////////////////////////////////////////////////////////////////
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click..." + url);

                if(url.contains("gdriveplayer.co") || url.contains("flashvid.net") || url.contains("pandora") ){
                    Log.i(TAG, "alert : executed shouldOverrideUrlLoading : " + url);
                    webView.stopLoading();
                    webView.loadUrl(url);
                }

                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                //super.onPageFinished(webView, url);
            }
        });

        Log.d(TAG, "AddHttpsBaseUrl : " + AddHttps(baseUrl));
        if(baseUrl.contains("pandora")){
            Toast.makeText(this,"pandora는 웹브라우저에서만 시청 가능합니다.", Toast.LENGTH_LONG);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(baseUrl));
            startActivity(intent);
            finish();
        } else {
            webView.loadUrl(AddHttps(baseUrl));
        }

    }

    public String AddHttps(String baseUrl){
        String resultUrl = baseUrl;
        if(!baseUrl.contains("http")) resultUrl = "https:" + resultUrl;

        return resultUrl;
    }
}
