package com.keren.tissuereplytv.item;

public class BeautyItem {
    private String ly_image;
    private String iv_left_image;
    private String memo_right1;
    private String memo_right2;
    private String memo_brand;
    private String memo_under;
    private String link;

    public BeautyItem(String ly_image, String iv_left_image, String memo_right1, String memo_right2, String memo_brand, String memo_under, String link) {
        this.ly_image = ly_image;
        this.iv_left_image = iv_left_image;
        this.memo_right1 = memo_right1;
        this.memo_right2 = memo_right2;
        this.memo_brand = memo_brand;
        this.memo_under = memo_under;
        this.link = link;
    }

    public String getLy_image() {
        return ly_image;
    }

    public void setLy_image(String ly_image) {
        this.ly_image = ly_image;
    }

    public String getIv_left_image() {
        return iv_left_image;
    }

    public void setIv_left_image(String iv_left_image) {
        this.iv_left_image = iv_left_image;
    }

    public String getMemo_right1() {
        return memo_right1;
    }

    public void setMemo_right1(String memo_right1) {
        this.memo_right1 = memo_right1;
    }

    public String getMemo_right2() {
        return memo_right2;
    }

    public void setMemo_right2(String memo_right2) {
        this.memo_right2 = memo_right2;
    }

    public String getMemo_brand() {
        return memo_brand;
    }

    public void setMemo_brand(String memo_brand) {
        this.memo_brand = memo_brand;
    }

    public String getMemo_under() {
        return memo_under;
    }

    public void setMemo_under(String memo_under) {
        this.memo_under = memo_under;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
