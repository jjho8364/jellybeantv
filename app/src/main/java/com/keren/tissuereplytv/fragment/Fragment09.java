package com.keren.tissuereplytv.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.keren.tissuereplytv.R;
import com.keren.tissuereplytv.activity.MidListActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class Fragment09 extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " Fragment09 - ";
    private ProgressDialog mProgressDialog;
    private ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";
    private List<String> pageUrlArr;
    private List<String> titleArr;

    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mid, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId(getResources().getString(R.string.full_start));
        interstitialAd.loadAd(adRequest);

        listView = (ListView)view.findViewById(R.id.listview);

        Log.d(TAG, "baseUrl : " + baseUrl);

        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);

        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        getListView = new GetListView();
        getListView.execute();

        return view;
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pageUrlArr = new ArrayList<String>();
            titleArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            Document docfirsturl = null;
            int realPage = pageNum == 1 ? 1 : (pageNum-1)*15;
            Log.d(TAG, "baseUrl : " + baseUrl + realPage);

            try {
                docfirsturl = Jsoup.connect("https://freekinglivetv.wordpress.com/kdrama2/").timeout(10000).get();
                doc = Jsoup.connect(baseUrl + realPage).timeout(10000).get();

                Elements lists = doc.select(".list-group a");
                String midFirstUrl = docfirsturl.select(".midfirsturl").text();

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).text();
                    String pageUrl = midFirstUrl + lists.get(i).attr("href");

                    titleArr.add(title);
                    pageUrlArr.add(pageUrl);
                    //Log.d(TAG, "pageUrl : " + pageUrl);
                }

                ////////////// get lat page /////////////
                Elements lis = doc.select(".pagination");
                if(lis.size() != 0){
                    String[] tempStrArr = lis.select("li").last().select("a").attr("href").split("/");
                    String lastString = tempStrArr[tempStrArr.length-1];
                    if(isStringDouble(lastString)){
                        tempLastPage = (Integer.parseInt(lastString)/15+1) + "";
                    } else {
                        tempLastPage = "1";
                    }
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tv_currentPage.setText(pageNum+"");
            tv_lastPage.setText(tempLastPage);

            if(getActivity() != null){
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titleArr);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.
                            interstitialAd.show();
                        } else {
                            Intent intent = new Intent(getActivity(), MidListActivity.class);
                            intent.putExtra("listUrl", pageUrlArr.get(position));
                            intent.putExtra("adsCnt", "0");
                            startActivity(intent);
                        }
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    new GetListView().execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    new GetListView().execute();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    public boolean isStringDouble(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
