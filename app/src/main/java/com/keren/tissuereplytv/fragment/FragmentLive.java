package com.keren.tissuereplytv.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.keren.tissuereplytv.R;
import com.keren.tissuereplytv.activity.LiveWebview;
import com.keren.tissuereplytv.adapter.GridDramaAdapter;
import com.keren.tissuereplytv.item.GridDramaItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentLive extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " FragmentLive - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private ArrayList<GridDramaItem> listViewItemArr;

    private int adsCnt = 0;
    SharedPreferences pref;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId(getResources().getString(R.string.full_start));
        interstitialAd.loadAd(adRequest);

        gridView = (GridView)view.findViewById(R.id.gridview);

        getGridView = new GetGridView();
        getGridView.execute();

        return view;
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridDramaItem>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).get();
                Elements lists = doc.select(".ccon .chan");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).select(".titl").text();
                    String update = lists.get(i).select(".prog").text();
                    String imgUrl = getImgUrl(lists.get(i).select("a.logo").attr("style"));
                    String listUrl = lists.get(i).select("a.logo").attr("href");

                    if(title.contains("허니")||title.contains("플레이보이")||title.contains("미드나잇")||title.contains("비키")) continue;

                    GridDramaItem gridViewItemList = new GridDramaItem(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridViewItemList);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                gridView.setAdapter(new GridDramaAdapter(getActivity(), listViewItemArr, R.layout.item_grid_drama));

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.
                            interstitialAd.show();
                        } else {
                            Log.d(TAG, "listUrl : " + listViewItemArr.get(position).getListUrl() );
                            Intent intent = new Intent(getActivity(), LiveWebview.class);
                            intent.putExtra("listUrl", listViewItemArr.get(position).getListUrl());
                            intent.putExtra("adsCnt", "0");
                            startActivity(intent);
                        }
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onClick(View v) {

    }

    public String getImgUrl(String fullStr) {
        String str = fullStr;
        String target1 = "(";
        String target2 = ")";
        int taget1Num = str.indexOf(target1);
        int taget2Num = str.indexOf(target2);

        return "https://" + str.substring(taget1Num+1, taget2Num);
    }
}
