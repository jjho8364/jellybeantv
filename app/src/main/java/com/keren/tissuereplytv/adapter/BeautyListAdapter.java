package com.keren.tissuereplytv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.keren.tissuereplytv.R;
import com.keren.tissuereplytv.item.BeautyItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BeautyListAdapter extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<BeautyItem> arrBeatyItem = null;
    private int nListCnt = 0;

    public BeautyListAdapter(ArrayList<BeautyItem> arrBeatyItem) {
        this.arrBeatyItem = arrBeatyItem;
        this.nListCnt = arrBeatyItem.size();
    }

    static class ViewHolder {
        public ImageView ly_image;
        public ImageView iv_left_image;
        public TextView memo_right1;
        public TextView memo_right2;
        public TextView memo_brand;
        public TextView memo_under;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();

            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.item_list_beauty, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.ly_image = (ImageView)convertView.findViewById(R.id.ly_image);
            viewHolder.iv_left_image = (ImageView)convertView.findViewById(R.id.iv_left_image);
            viewHolder.memo_right1 = (TextView)convertView.findViewById(R.id.memo_right1);
            viewHolder.memo_right2 = (TextView)convertView.findViewById(R.id.memo_right2);
            viewHolder.memo_brand = (TextView)convertView.findViewById(R.id.memo_brand);
            viewHolder.memo_under = (TextView)convertView.findViewById(R.id.memo_under);

            convertView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)convertView.getTag();
        BeautyItem item = arrBeatyItem.get(position);

        if(!item.getLy_image().equals("")) Picasso.with(parent.getContext()).load(item.getLy_image()).into(holder.ly_image);
        if(!item.getIv_left_image().equals("")) Picasso.with(parent.getContext()).load(item.getIv_left_image()).into(holder.iv_left_image);
        if(!item.getMemo_right1().equals("")) holder.memo_right1.setText(item.getMemo_right1());
        if(!item.getMemo_right2().equals("")) holder.memo_right2.setText(item.getMemo_right2());
        if(!item.getMemo_brand().equals("")) holder.memo_brand.setText(item.getMemo_brand());
        if(!item.getMemo_under().equals("")) holder.memo_under.setText(item.getMemo_under());

        return convertView;
    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return arrBeatyItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position ;
    }
}
